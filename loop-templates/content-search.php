<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>


<div class="col-6 col-md-4">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
		<div class="row align-items-center justify-content-center archive_news_block mt-2 mb-4">
			<div class="col-12 archive_news_headerimg">
				<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'archive-image-size' ); ?></a>
			</div>
			<div class="col-12 my-2 font-weight-bold archive_news_title">
				<?php
					the_title(
						sprintf( '<a href="%s" rel="bookmark" class="text-dark">', esc_url( get_permalink() ) ),
						'</a>'
					);
				?>
			</div>
			<div class="col-12"><p class="my-0 text-secondary font0_9"><?php echo get_the_excerpt(); ?></p></div>
		</div>
	</article>

</div>
