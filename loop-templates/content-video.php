<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<section class="section_block brand_bgwhite clearfix">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" class="py-5 position-relative">
		<!-- Header -->
		<header class="brand_bggray position-relative pt-4 pb-5">
			<div class="container">
			<div class="row align-items-center justify-content-center text-center">
				<div class="col-12">
					<?php the_title( '<h1 class="entry-title m-0 p-0 font1_6 font-weight-bold mb-4">', '</h1>' ); ?>
				</div>
				<div class="col-12 col-md-7 mb-3">
					<div class="embed-responsive embed-responsive-16by9">
						<?php the_field('youtube_link'); ?>
					</div>
				</div>
				<div class="col-7 mb-3">
					<?php the_content(); ?>
				</div>
			</div>
			</div>
		</header>

		<div class="container">
			<div class="row align-items-start justify-content-center my-5">
				<?php
				// WP_Query arguments
				$args = array (
					'post_type'              => array( 'videos' ),
					'post_status'            => array( 'publish' ),
					'nopaging'               => true,
					'order'                  => 'DESC',
					'orderby'                => 'date',
				);

				$services = new WP_Query( $args );

				if ( $services->have_posts() ) {
					while ( $services->have_posts() ) {
						$services->the_post();
				?>
									<?php

									/*
										* Include the Post-Format-specific template for the content.
										* If you want to override this in a child theme, then include a file
										* called content-___.php (where ___ is the Post Format name) and that will be used instead.
										*/
									get_template_part( 'loop-templates/content', 'videos' );
									?>
				<?php		
					}
				} else {
				?>
				<?php
				}
				// Restore original Post Data
				wp_reset_postdata();
				?>
			</div>
		</div>

	</article>
</section>