<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

	--><li class="gridder-list my-3" data-griddercontent="#post-<?php the_ID(); ?>">
			<div class="row align-items-center justify-content-center mx-0">
				<div class="col-12">
					<div class="row align-items-center justify-content-center mx-auto">
						<div class="col-12 hackathon_logoblock p-2 pt-4 p-md-4 text-center">
							<div class="hackathon_logothumbnail mx-auto my-auto">
							<?php echo get_the_post_thumbnail( $post->ID, 'icons' ); ?>
							</div>
							<div class="hackathon_label mt-2">
                        <div class="font-weight-bold font1_2"><?php the_title(); ?></div>
                        <div class="font-weight-bold font0_8">&nbsp;<?php the_field('appname_hackathon'); ?>&nbsp;</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li><!--