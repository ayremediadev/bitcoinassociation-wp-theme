<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="col-6 col-sm-6 col-md-4 col-lg-3 pt-5">
   <div class="row align-items-start justify-content-center mx-0 shadow-lg p-3 my-5 bg-white rounded">
      <div class="col-auto text-center my-3">
         <div class="boardmember_photo">
			<?php
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						the_post_thumbnail( 'photo-id', array( 'class'  => 'responsive-class img-fluid' ) ); // show featured image
				} 
			?>   
         </div>
      </div>
      <div class="col-12 text-center">
         <div class="boardmember_name my-2 font-weight-bold"><?php echo get_the_title(); ?></div>
         <p class="font0_9"><?php echo get_the_content(); ?></p>
      </div>
   </div>
</div>