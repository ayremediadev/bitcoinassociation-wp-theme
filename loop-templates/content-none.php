<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
$container = get_theme_mod( 'understrap_container_type' );
?>

<main role="main" class="clearfix">
	<section class="section_block brand_bgyellow clearfix">
		<!-- Header -->
		<header class="position-relative pt-4 pb-5">
			<div class="<?php echo esc_attr( $container ); ?>" tabindex="-1">
            <div class="row align-items-center justify-content-center text-center text-white">
					<h1 class="display-1 text-uppercase font-weight-bold b-block"><?php esc_html_e( 'Nothing Found', 'understrap' ); ?></h1>
				</div>
            <div class="row align-items-center justify-content-center text-center text-white">
					<h2 class="d-block"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'understrap' ); ?></h2>
            </div>
			</div>
		</header>
	</section>
	<section class="section_block brand_bgyellow pb-5">
		<div class="<?php echo esc_attr( $container ); ?>" tabindex="-1">
			<div class="row align-items-center justify-content-center text-center text-white">
				<div class="col-7 mb-3 text-white text-center">
					<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'understrap' ); ?></p>
				</div>
			</div>
		</div>
	</div>
</main>

<?php if ( get_field( 'display_welcome_video' ) ): ?>
   <section class="section_block brand_bgyellow py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center">
               <div class="col-12 mb-4"><h2 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('last_call_welcome_title', 'option'); ?></h2></div>
               <div class="col-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('last_call_welcome_video', 'option'); ?>
                  </div>
               </div>
         </div>
      </div>
   </section>
<?php endif; ?>

   <section class="section_block brand_bgwhite py-5">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md">
               <p class="p-0 m-0"><?php the_field('join_now_content_footer', 'option', false, false); ?></p>
            </div>
            <div class="col-12 col-md-auto mx-auto">
            <?php
            $joinmembership_url = get_field('join_now_cta_url_footer', 'option');
            ?>
            <a href="<?php echo get_permalink( $joinmembership_url ); ?>" class="btn btn-invert shadow-sm mx-auto text-center"><?php the_field('join_now_cta_label_footer', 'option'); ?></a>
            </div>
         </div>
      </div>
   </section>
