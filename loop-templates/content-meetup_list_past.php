<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$date = get_field('event_date_start');

?>
	--><tr>
       <td><a href="<?php the_field('event_url'); ?>" target="_blank"><?php the_title(); ?></a></td>
  	   <td><?php the_field('event_location'); ?></td>
  	   <td><?php echo date('F j, Y', strtotime($date));  ?></td>
     </tr><!--