<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

	--><li class="gridder-list my-3" data-griddercontent="#post-<?php the_ID(); ?>">
			<div class="row align-items-center justify-content-center mx-0">
				<div class="col-12">
					<div class="row align-items-center justify-content-center mx-auto">
						<div class="col-12 ecosystem_logoblock p-2 pt-4 p-md-4 text-center">
							<div class="ecosystem_logothumbnail mx-auto my-auto">
							<?php echo get_the_post_thumbnail( $post->ID, 'icons' ); ?>
							</div>
							<div class="ecosystem_label mt-2">
								<div class="font-weight-bold font1_2"><?php the_title(); ?></div>
								<a href="<?php the_field('url_ecosystem'); ?>" target="_blank" class="brand_txtblue"><?php the_field('url_label_ecosystem'); ?></a>
							</div>
						</div>
						<div class="col-12 ecosystem_excerpt p-3 font0_85 d-none d-sm-block">
							<p><?php echo get_the_excerpt(); ?></p>
						</div>
					</div>
				</div>
			</div>
		</li><!--