<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div id="post-<?php the_ID(); ?>" class="gridder-content">
   <div class="row align-items-start justify-content-center p-3 text-white">
      <!-- <div class="col-12 col-md-3 text-center mx-auto">
         <a href="<?php the_field('url_hackathon'); ?>" class="imageavatar_border text-center mx-auto">
            <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
         </a>
      </div> -->
      <!-- <div class="col-12 col-md-auto">
         <hr class="vr_divider m-0" />
      </div> -->
      <div class="col-12 col-md text-left">
         <h2 class="font-weight-bold font1_4 my-0"><?php the_title(); ?></h2>
         <!-- <div class="my-1"><i class="fa fa-globe font1_2" aria-hidden="true"></i>&nbsp; &nbsp;<a href="<?php the_field('url_hackathon'); ?>" target="_blank" class="brand_txtyellow font0_9 mb-3"><?php the_field('url_hackathon'); ?></a></div>
         <div class="my-1"><i class="fa fa-github font1_2" aria-hidden="true"></i>&nbsp; &nbsp;<a href="<?php the_field('github_hackathon'); ?>" target="_blank" class="brand_txtyellow font0_9 mb-3"><?php the_field('github_hackathon'); ?></a></div> -->
         <!-- <h3 class="font-weight-bold font1_2 my-0"><?php the_field('appname_hackathon'); ?></h3> -->
         <div class="mt-3">
            <?php the_content(); ?>
         </div>
      </div>
   </div>
</div>