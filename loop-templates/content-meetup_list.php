<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$date = get_field('event_date_start');

?>

	--><li class="gridder-list my-3">
		<div class="event-card mx-3 d-flex align-items-end"><a href="<?php the_field('event_url'); ?>" target="_blank">
				<div class="location text-white px-4 pt-4 mb-2 text-uppercase font1_4"><?php the_field('event_location'); ?></div>
				<div class="date text-white px-4 pb-4 font1_2"><?php  echo date('F d', strtotime($date));  ?></div>
				<div class="bg" style="background: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>') no-repeat bottom center"></div></a>
		</div>
		</li><!--