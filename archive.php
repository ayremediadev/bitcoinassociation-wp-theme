<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>
<main role="main" class="clearfix">

<?php if ( have_posts() ) : ?>

	<!-- Header -->
	<header class="brand_bgyellow position-relative">
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row align-items-center justify-content-center text-center">
				<div class="col-12">
						<?php
							//the_archive_title( '<h1 class="page-title">', '</h1>' );
							//the_archive_description( '<div class="taxonomy-description">', '</div>' );
						?>
				</div>
			</div>
		</div>
	</header>
	<!-- Page Content -->
	<section class="section_block brand_bgwhite py-5">
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row align-items-start justify-content-center">
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php

					/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
					get_template_part( 'loop-templates/content', 'news' );
					?>

				<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

				</div>
				<div class="row align-items-center justify-content-center my-5">
					<div class="col-12">
					<!-- The pagination component -->
					<?php understrap_pagination(); ?>

					</div>
				</div>

      </div>
    </section>
</main>
<?php if ( get_field( 'display_welcome_video' ) ): ?>
   <section class="section_block brand_bgyellow py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center">
               <div class="col-12 mb-4"><h2 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('last_call_welcome_title', 'option'); ?></h2></div>
               <div class="col-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('last_call_welcome_video', 'option'); ?>
                  </div>
               </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php if ( get_field( 'display_membership_bar' ) ): ?>
   <section class="section_block brand_bggray py-5">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md">
               <p class="p-0 m-0"><?php the_field('join_now_content_footer', 'option', false, false); ?></p>
            </div>
            <div class="col-12 col-md-auto">
            <?php
            $joinmembership_url = get_field('join_now_cta_url_footer', 'option');
            ?>
            <a href="<?php echo get_permalink( $joinmembership_url ); ?>" class="btn btn-invert shadow-sm"><?php the_field('join_now_cta_label_footer', 'option'); ?></a>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php get_footer(); ?>
