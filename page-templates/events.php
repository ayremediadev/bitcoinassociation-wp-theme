<?php
/**
 * Template Name: Events Page
 *
 * Template for displaying a page without sidebar customizeable header and content.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<main role="main" class="clearfix events-page">
   <?php while ( have_posts() ) : the_post(); ?>
   
   <!-- Header -->
   <?php if( get_field('event_header') ): ?>
   <header class="position-relative py-5" style="background-color: #eab300">
      <?php the_field('event_header', false, false); ?>
   </header>
   <?php endif; ?>   
      

   <div class="position-relative py-5 mb-5" style="background-color: #f9f9f2;"><div class="col-12 text-center"><h1  id="events"  class="text-uppercase m-0 p-0 font1_6 font-weight-bold">Events</h1></div></div>

   <section class="section_block py-5 events">

      <div class="<?php echo esc_attr( $container ); ?>">
         <h2 class="text-uppercase font-weight-normal font1_6" style="padding-left: 15px">Upcoming</h2>

         <ul class="events-list position-relative"><!--
         <?php
            $term = get_field('event');
            $termslug = $term->slug;
            $today = current_time( 'Ymd');

            

            $the_query = new WP_Query( array(
               'post_type' => 'Events',
               // 'posts_per_page'  => 10,
               'order' => 'ASC',
               'orderby'   => 'meta_value_num',
               'meta_key'  => 'event_date_start',
               'meta_query' => array(
                     array(
                        'key'     => 'event_date_start',
                        'value' => $today,
                        'compare' => '>=',
                        'type' => 'NUMERIC',
                     ),
                  ),
               'tax_query' => array(
                  array (
                        'taxonomy' => 'event_type',               
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
               get_template_part( 'loop-templates/content', 'event_list' );

            endwhile;
            wp_reset_postdata();
         ?>
      --></ul>
         
      </div>

   </section>

   <section class="section_block py-5">
      <div class="<?php echo esc_attr( $container ); ?>">
         <h2 class="text-uppercase font-weight-normal font1_6" style="padding-left: 15px">Past Events</h2>

         <table class="table">
           <thead class="thead-dark">
             <tr>
               <th style="width: 40%">Event</th>
               <th style="width: 30%">Location</th>
               <th>Date</th>
             </tr>
           </thead>
           <tbody><!--
         <?php
            $term = get_field('event');
            $termslug = $term->slug;
            $today = current_time( 'Ymd');

            $the_query = new WP_Query( array(
               'post_type' => 'Events',
               // 'posts_per_page'  => 10,
               'order' => 'DESC',
               'orderby'   => 'meta_value_num',
               'meta_key'  => 'event_date_start',
               'meta_query' => array(
                     array(
                        'key'     => 'event_date_start',
                        'value' => $today,
                        'compare' => '<',
                        'type' => 'NUMERIC',
                     ),
                  ),
               'tax_query' => array(
                  array (
                        'taxonomy' => 'event_type',               
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
               get_template_part( 'loop-templates/content', 'event_list_past' );

            endwhile;
            wp_reset_postdata();
         ?>
      -->
           </tbody>
         </table>

      </div>
   </section>

   <div class="position-relative py-5 mb-5" style="background-color: #f9f9f2;"><div class="col-12 text-center"><h1  id="meetups"  class="text-uppercase m-0 p-0 font1_6 font-weight-bold">Meetups</h1></div></div>

   <section class="section_block py-5 meetups">

      <div class="<?php echo esc_attr( $container ); ?>">
         <h2 class="text-uppercase font-weight-normal font1_6" style="padding-left: 15px">Upcoming</h2>

         <ul class="events-list  position-relative"><!--
         <?php
            $term = get_field('meetup');
            $termslug = $term->slug;
            $today = current_time( 'Ymd');

            $the_query = new WP_Query( array(
               'post_type' => 'Events',
               'posts_per_page'  => 8,
               'order' => 'ASC',
               'orderby'   => 'meta_value_num',
               'meta_key'  => 'event_date_start',
               'meta_query' => array(
                     array(
                        'key'     => 'event_date_start',
                        'value' => $today,
                        'compare' => '>=',
                        'type' => 'NUMERIC',
                     ),
                  ),
               'tax_query' => array(
                  array (
                        'taxonomy' => 'event_type',
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
               get_template_part( 'loop-templates/content', 'meetup_list' );

            endwhile;
            wp_reset_postdata();
         ?>
      --></ul>
         
      </div>

   </section>  

   <section class="section_block py-5">
      <div class="<?php echo esc_attr( $container ); ?>">
         <h2 class="text-uppercase font-weight-normal font1_6" style="padding-left: 15px">Past Meetups</h2>

         <table class="table">
           <thead class="thead-dark">
             <tr>
               <th style="width: 40%">Event</th>
               <th style="width: 30%">Location</th>
               <th>Date</th>
             </tr>
           </thead>
           <tbody><!--
         <?php
            $term = get_field('meetup');
            $termslug = $term->slug;
            $today = current_time( 'Ymd');

            $the_query = new WP_Query( array(
               'post_type' => 'Events',
               // 'posts_per_page'  => 10,
               'order' => 'DESC',
               'orderby'   => 'meta_value_num',
               'meta_key'  => 'event_date_start',
               'meta_query' => array(
                     array(
                        'key'     => 'event_date_start',
                        'value' => $today,
                        'compare' => '<',
                        'type' => 'NUMERIC',
                     ),
                  ),
               'tax_query' => array(
                  array (
                        'taxonomy' => 'event_type',               
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
               get_template_part( 'loop-templates/content', 'meetup_list_past' );

            endwhile;
            wp_reset_postdata();
         ?>
      -->
           </tbody>
         </table>

      </div>
   </section>

   <div class="position-relative py-5 mb-5" style="background-color: #f9f9f2;"><div class="col-12 text-center"><h1 class="text-uppercase m-0 p-0 font1_6 font-weight-bold">Get Involved</h1></div></div>

 <section class="section_block py-5">
      <div class="<?php echo esc_attr( $container ); ?>">
         <div style="margin-left: 15px; margin-right: 15px; margin-bottom: 40px;">
            <?php the_content(); ?>
         </div>
      </div>
   </section>

   
   
   <?php endwhile; // end of the loop. ?>

</main>

<?php get_footer(); ?>
