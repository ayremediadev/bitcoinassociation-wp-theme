<?php
/**
 * Template Name: Ecosystem Page
 *
 * Template for displaying a page without sidebar customizeable header and content.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<main role="main" class="clearfix">
   <?php while ( have_posts() ) : the_post(); ?>
   <!-- Header -->
	<header class="brand_bgwht position-relative py-5">
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row align-items-center justify-content-center text-center">
				<div class="col-12">
               <h1 class="text-uppercase mb-5 p-0 font1_6 font-weight-bold"><?php echo get_the_title(); ?></h1>
				</div>
				<div class="col-12">
               <?php the_content(); ?>
				</div>
			</div>
		</div>
	</header>

   <section class="section_block brand_bgwhite py-5 clearfix">
      <div class="<?php echo esc_attr( $container ); ?> clearfix">



        <ul class="gridder position-relative"><!--
         <?php
            $term = get_field('ecosystem_category');
            $termslug = $term->slug;
            ?>
            <?php
            $the_query = new WP_Query( array(
               'post_type' => 'Ecosystem',
               'posts_per_page'  => 20,
               'order' => 'ASC',
               'orderby' => 'name',
               'tax_query' => array(
                  array (
                        'taxonomy' => 'ecosystems_list',
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
            ?>
               <?php
               get_template_part( 'loop-templates/content', 'ecosystem_list' );
               ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
      --></ul>
         <?php
            $term = get_field('ecosystem_category');
            $termslug = $term->slug;
            ?>
            <?php
            $the_query = new WP_Query( array(
               'post_type' => 'Ecosystem',
               'tax_query' => array(
                  array (
                        'taxonomy' => 'ecosystems_list',
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
            ?>
               <?php
               get_template_part( 'loop-templates/content', 'ecosystem_description' );
               ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
      </div>
      <div class="<?php echo esc_attr( $container ); ?> clearfix">
         <div class="row align-items-center justify-content-center my-3 clearfix">
            <div class="col-12">
               <?php the_field('ecosystem_footer_content'); ?>
            </div>
         </div>
      </div>
   </section>

<?php if ( get_field( 'display_welcome_video' ) ): ?>
   <section class="section_block brand_bgyellow py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center">
               <div class="col-12 mb-4"><h2 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('last_call_welcome_title', 'option'); ?></h2></div>
               <div class="col-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('last_call_welcome_video', 'option'); ?>
                  </div>
               </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php if ( get_field( 'display_membership_bar' ) ): ?>
   <section class="section_block brand_bggray py-5">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md">
               <p class="p-0 m-0"><?php the_field('join_now_content_footer', 'option', false, false); ?></p>
            </div>
            <div class="col-12 col-md-auto">
            <?php
            $joinmembership_url = get_field('join_now_cta_url_footer', 'option');
            ?>
            <a href="<?php echo get_permalink( $joinmembership_url ); ?>" class="btn btn-invert shadow-sm"><?php the_field('join_now_cta_label_footer', 'option'); ?></a>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>
    <?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>
