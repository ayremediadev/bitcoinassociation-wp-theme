<?php
/**
 * Template Name: About Us Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>


<main role="main" class="clearfix">
   <?php while ( have_posts() ) : the_post(); ?>
   <!-- Header -->
   <header class="brand_bgwht position-relative py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center text-center">
            <div class="col-12">
               <h1 class="text-uppercase m-0 p-0 font1_6 font-weight-bold"><?php the_field('about_us_title'); ?></h1>
            </div>
         </div>
      </div>
   </header>
   <section class="section_block brand_bgwhite">
      <div class="<?php echo esc_attr( $container ); ?>">
         <div class="row align-items-start justify-content-center my-2">
            <?php 
            $aboutusservices = get_field('about_us_service');
            if( $aboutusservices ): ?>
               <?php foreach( $aboutusservices as $post): // variable must be called $post (IMPORTANT) ?>
                  <?php setup_postdata($post); ?>
                  <?php get_template_part( 'loop-templates/content', 'services' ); ?>
               <?php endforeach; ?>
               <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
         </div>
      </div>
      <div class="<?php echo esc_attr( $container ); ?>">
         <div class="row align-items-center justify-content-center">
            <div class="col-auto">
               <?php
               $becomemember_url = get_field('become_a_member_page');
               ?>
               <a class="btn btn-invert btn-lg text-uppercase shadow-sm" href="<?php echo get_permalink( $becomemember_url ); ?>"><span><?php the_field('become_a_member_label'); ?></span></a>
            </div>
         </div>
      </div>
      <div class="<?php echo esc_attr( $container ); ?>">
         <div class="row align-items-center justify-content-center py-5">
            <div class="col-12">
               <h1 class="text-uppercase m-0 p-0 font1_6 font-weight-normal text-center"><?php the_field('board_of_directors_title'); ?></h1>
            </div>
         </div>
         <div class="row align-items-start justify-content-center">
            <?php 
            $topchair = get_field('board_of_directors_top');
            if( $topchair ): ?>
               <?php foreach( $topchair as $post): // variable must be called $post (IMPORTANT) ?>
                  <?php setup_postdata($post); ?>
                  <?php get_template_part( 'loop-templates/content', 'directors' ); ?>
               <?php endforeach; ?>
               <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
         </div>
         <div class="row align-items-start justify-content-center">
            <?php 
            $boardofdirectors = get_field('board_of_directors_list');
            if( $boardofdirectors ): ?>
               <?php foreach( $boardofdirectors as $post): // variable must be called $post (IMPORTANT) ?>
                  <?php setup_postdata($post); ?>
                  <?php get_template_part( 'loop-templates/content', 'directors' ); ?>
               <?php endforeach; ?>
               <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
         </div>
      </div>
   </section>
<?php if ( get_field( 'display_welcome_video' ) ): ?>
   <section class="section_block brand_bgyellow py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center">
               <div class="col-12 mb-4"><h2 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('last_call_welcome_title', 'option'); ?></h2></div>
               <div class="col-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('last_call_welcome_video', 'option'); ?>
                  </div>
               </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php if ( get_field( 'display_membership_bar' ) ): ?>
   <section class="section_block brand_bggray py-5">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md">
               <p class="p-0 m-0"><?php the_field('join_now_content_footer', 'option', false, false); ?></p>
            </div>
            <div class="col-12 col-md-auto">
            <?php
            $joinmembership_url = get_field('join_now_cta_url_footer', 'option');
            ?>
            <a href="<?php echo get_permalink( $joinmembership_url ); ?>" class="btn btn-invert shadow-sm"><?php the_field('join_now_cta_label_footer', 'option'); ?></a>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>


    <?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>
