<?php
/**
 * Check and setup theme's default settings
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'understrap_setup_theme_default_settings' ) ) {
	function understrap_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$understrap_posts_index_style = get_theme_mod( 'understrap_posts_index_style' );
		if ( '' == $understrap_posts_index_style ) {
			set_theme_mod( 'understrap_posts_index_style', 'default' );
		}

		// Sidebar position.
		$understrap_sidebar_position = get_theme_mod( 'understrap_sidebar_position' );
		if ( '' == $understrap_sidebar_position ) {
			set_theme_mod( 'understrap_sidebar_position', 'right' );
		}

		// Container width.
		$understrap_container_type = get_theme_mod( 'understrap_container_type' );
		if ( '' == $understrap_container_type ) {
			set_theme_mod( 'understrap_container_type', 'container' );
		}
	}
}
// add_filter( 'theme_page_templates', 'child_remove_page_templates' );
// function child_remove_page_templates( $page_templates ) {
// 	unset( $page_templates['page-templates/aboutus.php'] );
// 	unset( $page_templates['page-templates/archive.php'] );
// 	unset( $page_templates['page-templates/blank.php'] );
// 	unset( $page_templates['page-templates/customize-fullpage.php'] );
// 	unset( $page_templates['page-templates/ecosystem.php'] );
// 	unset( $page_templates['page-templates/empty.php'] );
// 	unset( $page_templates['page-templates/homepage.php'] );
// 	unset( $page_templates['page-templates/video.php'] );

// 	return $page_templates;
// }