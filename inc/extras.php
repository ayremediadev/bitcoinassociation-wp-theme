<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter( 'body_class', 'understrap_body_classes' );

if ( ! function_exists( 'understrap_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function understrap_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'understrap_adjust_body_class' );

if ( ! function_exists( 'understrap_adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function understrap_adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'understrap_change_logo_class' );

if ( ! function_exists( 'understrap_change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function understrap_change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"', $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */

if ( ! function_exists ( 'understrap_post_nav' ) ) {
	function understrap_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="container navigation post-navigation">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'understrap' ); ?></h2>
			<div class="row nav-links justify-content-between">
				<?php
				if ( get_previous_post_link() ) {
					previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'understrap' ) );
				}
				if ( get_next_post_link() ) {
					next_post_link( '<span class="nav-next">%link</span>', _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'understrap' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}

if ( ! function_exists( 'understrap_pingback' ) ) {
	/**
	 * Add a pingback url auto-discovery header for single posts of any post type.
	 */
	function understrap_pingback() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="' . esc_url( get_bloginfo( 'pingback_url' ) ) . '">' . "\n";
		}
	}
}
add_action( 'wp_head', 'understrap_pingback' );

if ( ! function_exists( 'understrap_mobile_web_app_meta' ) ) {
	/**
	 * Add mobile-web-app meta.
	 */
	function understrap_mobile_web_app_meta() {
		echo '<meta name="mobile-web-app-capable" content="yes">' . "\n";
		echo '<meta name="apple-mobile-web-app-capable" content="yes">' . "\n";
		echo '<meta name="apple-mobile-web-app-title" content="' . esc_attr( get_bloginfo( 'name' ) ) . ' - ' . esc_attr( get_bloginfo( 'description' ) ) . '">' . "\n";
	}
}
add_action( 'wp_head', 'understrap_mobile_web_app_meta' );

// Creates Game Reviews Custom Post Type
function directors_init() {
	$args = array(
		'label' => 'Directors',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'directors'),
			'query_var' => true,
			'menu_icon' => 'dashicons-businessman',
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'directors', $args );
}
add_action( 'init', 'directors_init' );

// Creates Videos Custom Post Type
function videos_init() {
	$args = array(
		'label' => 'Videos',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'videos'),
			'query_var' => true,
			'menu_icon' => 'dashicons-video-alt3',
			'taxonomies' => array('post_tag'),
			'all_items' => __( 'All Videos' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Video' ), 
			'update_item' => __( 'Update Video' ),
			'add_new_item' => __( 'Add New Video' ),
			'new_item_name' => __( 'New Video Name' ),
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'videos', $args );
}
add_action( 'init', 'videos_init' );


// Creates Ecosystem Custom Post Type
function ecosystem_init() {
	$args = array(
		'label' => 'Ecosystem',
			'public' => false,
			'show_ui' => true,
			'capability_type' => 'page',
			'hierarchical' => true,
			'rewrite' => array('slug' => 'ecosystems'),
			'taxonomies' => array( 'ecosystems_list' ),
			'query_var' => true,
			'menu_icon' => 'dashicons-groups',
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'ecosystem', $args );
}
add_action( 'init', 'ecosystem_init' );

function customposttype_taxonomy() {  
	register_taxonomy(  
		 'ecosystems_list',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
		 'ecosystem',        //post type name
		 array(  
			  'hierarchical' => true,  
			  'label' => 'Ecosystems List',  //Display name
			  'query_var' => true,
			  'rewrite' => array(
					'slug' => 'list', // This controls the base slug that will display before each term
					'with_front' => false // Don't display the category base before 
			  )
		 )  
	);  
}  
add_action( 'init', 'customposttype_taxonomy');

// Creates Services Custom Post Type
function services_init() {
	$args = array(
		'label' => 'Services',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'services'),
			'query_var' => true,
			'menu_icon' => 'dashicons-screenoptions',
			'all_items' => __( 'All Services' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Service' ), 
			'update_item' => __( 'Update Service' ),
			'add_new_item' => __( 'Add New Service' ),
			'new_item_name' => __( 'New Service Name' ),
			'supports' => array(
					'title',
					'editor',
					'revisions',
					'thumbnail',

					'page-attributes',)
			);
	register_post_type( 'services', $args );
}
add_action( 'init', 'services_init' );

// Creates Game Reviews Custom Post Type
function hackathonfinalist_init() {
	$args = array(
		'label' => 'Hackathon Finalist',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'hackathonfinalist'),
			'query_var' => true,
			'menu_icon' => 'dashicons-awards',
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'hackathonfinalist', $args );
}
add_action( 'init', 'hackathonfinalist_init' );



function my_taxonomy_query( $args, $field, $post_id ) {
    
	// modify args
	$args['orderby'] = 'count';
	$args['order'] = 'ASC';
	
	
	// return
	return $args;
	
}

add_filter('acf/fields/taxonomy/query', 'my_taxonomy_query', 10, 3);

add_shortcode( 'hackathon_finalist', 'hackathonfinalist_display' );
function hackathonfinalist_display( $atts ) {
	ob_start();
	$hackathonfinalist = get_field('hackathonfinalist_query', false, false);
	$query = new WP_Query( array(
		'post_type' 		=> 'hackathonfinalist',
		'posts_per_page' 	=> 20,
		'post__in'			=> $hackathonfinalist,
		'post_status'		=> 'published',
		'orderby' 			=> 'post__in',
	) );
	if ( $query->have_posts() ) { ?>
		<ul class="gridder position-relative transparentbg_noborder"><!--
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part( 'loop-templates/content', 'hackathon_list' ); ?>
			<?php endwhile;
			wp_reset_postdata(); ?>
		--></ul>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part( 'loop-templates/content', 'hackathon_description' ); ?>
			<?php endwhile;
			wp_reset_postdata(); ?>
	<?php $myvariable = ob_get_clean();
	return $myvariable;
	}
}


add_shortcode( 'hackathon_participants', 'hackathonparticipants_display' );
function hackathonparticipants_display( $atts ) {
	ob_start();
	$hackathonparticipants = get_field('hackathonparticipants_query', false, false);
	$query = new WP_Query( array(
		'post_type' 		=> 'hackathonfinalist',
		'posts_per_page' 	=> 20,
		'post__in'			=> $hackathonparticipants,
		'post_status'		=> 'published',
		'orderby' 			=> 'post__in',
	) );
	if ( $query->have_posts() ) { ?>
		<ul class="gridder position-relative transparentbg_noborder"><!--
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part( 'loop-templates/content', 'hackathon_list' ); ?>
			<?php endwhile;
			wp_reset_postdata(); ?>
		--></ul>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part( 'loop-templates/content', 'hackathon_description' ); ?>
			<?php endwhile;
			wp_reset_postdata(); ?>
	<?php $myvariable = ob_get_clean();
	return $myvariable;
	}
}

// Creates Events Custom Post Type
function events_init() {
	$args = array(
		'label' => 'Events',
			'public' => false,
			'show_ui' => true,
			'capability_type' => 'page',
			'hierarchical' => true,
			'rewrite' => array('slug' => 'events'),
			'taxonomies' => array( 'event_type' ),
			'query_var' => true,
			'menu_icon' => 'dashicons-calendar-alt',
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'events', $args );
}
add_action( 'init', 'events_init' );

function events_taxonomy() {  
	register_taxonomy(  
		 'event_type',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
		 'events',        //post type name
		 array(  
			  'hierarchical' => true,  
			  'label' => 'Event Type',  //Display name
			  'query_var' => true,
			  'rewrite' => array(
					'slug' => 'list', // This controls the base slug that will display before each term
					'with_front' => false // Don't display the category base before 
			  )
		 )  
	);  
}  
add_action( 'init', 'events_taxonomy');


// Add the custom column to the post type
add_filter( 'manage_events_posts_columns', 'events_add_custom_column' );
function events_add_custom_column( $columns ) {
    $columns['event_type'] = 'Event Type';
    return $columns;
}

// Add the data to the custom column
add_action( 'manage_events_posts_custom_column' , 'events_add_custom_column_data', 10, 2 );
function events_add_custom_column_data( $column, $post_id ) {
    switch ( $column ) {
        case 'event_type' :
            $terms =  get_the_term_list( $post_id , 'event_type' , '' , ', ' , '' ); 
            echo strip_tags($terms);
            break;
    }
}

// Make the custom column sortable
add_filter( 'manage_edit-events_sortable_columns', 'events_add_custom_column_make_sortable' );
function events_add_custom_column_make_sortable( $columns ) {
	$columns['event_type'] = '_events_event_type';
	return $columns;
}

// Add custom column sort request to post list page
add_action( 'load-edit.php', 'events_add_custom_column_sort_request' );
function events_add_custom_column_sort_request() {
	add_filter( 'request', 'events_add_custom_column_do_sortable' );
}

// Handle the custom column sorting
function events_add_custom_column_do_sortable( $vars ) {

	// check if post type is being viewed
	if ( isset( $vars['post_type'] ) && 'events' == $vars['post_type'] ) {

		// check if sorting has been applied
		if ( isset( $vars['orderby'] ) && 'event_type' == $vars['orderby'] ) {

			// apply the sorting to the post list
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => '_events_event_type',
					'orderby' => 'meta_value_num'
				)
			);
		}
	}

	return $vars;
}
